#include <iostream>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <wait.h>
#include <sys/sem.h>

#include "ipcwrapper.h"
	
int IPCWrapper::createSemaphore(int initVal) {
	
	int *sharedMemoryId = (int *)getSharedMemoryPointer(refToSharedMemoryId);
	
	int semId = semget((*sharedMemoryId)++, 1, IPC_CREAT | 0666);
	semctl(semId, 0, SETVAL, initVal);
	
	return semId;
	
	}
int IPCWrapper::createSemaphore() {
	
	int *sharedMemoryId = (int *)getSharedMemoryPointer(refToSharedMemoryId);
	
	int semId = semget((*sharedMemoryId)++, 1, IPC_CREAT | 0666);
	semctl(semId, 0, SETVAL, 1);
	
	return semId;
	
	}
	
void IPCWrapper::releaseSemaphore(int semId) {
	
	struct sembuf bufRelease;
	
	bufRelease.sem_num = 0; //numer semafora
	bufRelease.sem_op = 1;  //podniesienie semafora o 1 stopien
	bufRelease.sem_flg = 0; //bez flag
	
	semop(semId, &bufRelease, 1);
	
	}
	
void IPCWrapper::acquireSemaphore(int semId) {
	
	struct sembuf bufAcquire;
	
	bufAcquire.sem_num = 0; //numer semafora
	bufAcquire.sem_op = -1;  //podniesienie semafora o 1 stopien
	bufAcquire.sem_flg = 0; //bez flag
	
	semop(semId, &bufAcquire, 1);
	
	}
	

/*

//w sumie niepotrzeba funkcja
//procesy beda sobie przekazywac semId, a nie key

int getSemaphoreId(int key) {
	
	int semId = semget(key, 1, IPC_EXCL);
	return semId;
	
	}
*/

void * IPCWrapper::getSharedMemoryPointer(int shmId) {

	void *shmPointer = shmat(shmId, NULL, 0);	
	return shmPointer;
	
	}
	
int IPCWrapper::createSharedMemory(size_t size){
	
	int counter = 0;
	int result = 0;
	
	do {
		result = createSharedMemoryInternal(size);
		counter++;
	} while(result == -1 && counter < 50);
	
		if(result == -1)std::cout << "Error: could not allocate shared memory" << std::endl;
		
		return result;
	}

int IPCWrapper::createSharedMemoryInternal(size_t size){
	
	int *sharedMemoryId = (int *)getSharedMemoryPointer(refToSharedMemoryId);
	
	acquireSemaphore(refToSemaphoreId);
		int shmId = shmget((*sharedMemoryId)++, size, IPC_CREAT | 0666);
		
		//std::cout << "shmId = " << shmId << " sharedMemoryId=" << *sharedMemoryId << std::endl;
		
		if(shmId == -1) {
			 releaseSemaphore(refToSemaphoreId);
			 return -1;
			}
			
		releaseSemaphore(refToSemaphoreId);
		return shmId;
	}
	
IPCWrapper::IPCWrapper() {
		int shmId = shmget(23000, sizeof(int), IPC_CREAT | 0666);
		if(shmId == -1)std::cout << "invalid shmid" << std::endl;
		void *shmPointer = shmat(shmId, NULL, 0);
		int *sharedMemoryId = (int *)shmPointer;

		//int shmId2 = shmget(23005, sizeof(int), IPC_CREAT | 0666);
		//if(shmId2 == -1)std::cout << "invalid shmid" << std::endl;
		//void *shmPointer2 = shmat(shmId2, NULL, 0);
		//int *semaphoreId = (int *)shmPointer2;
		
		int semId = semget(23100, 1, IPC_CREAT | 0666);
		if(semId == -1)std::cout << "invalid semid" << std::endl;
		semctl(semId, 0, SETVAL, 1);
		
		
		*sharedMemoryId = 44456;
		refToSharedMemoryId = shmId;
		refToSemaphoreId = semId;
	}
IPCWrapper::~IPCWrapper() {
	
	}
