//network includes
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<arpa/inet.h>
#include<netdb.h>
//other includes
#include<stdio.h>
#include<iostream>
#include<unistd.h>
#include<stdlib.h>
#include<signal.h>

#include "tcpsocket.h"


TCPSocket::TCPSocket()
{	
}

//Creates descriptor.
int TCPSocket::init()
{
	fd = socket(AF_INET, SOCK_STREAM,0);
	return fd;
}

int TCPSocket::bind(int port)
{	
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(port);
	servAddr.sin_addr.s_addr = htonl(INADDR_ANY);
	
	return ::bind(fd, (struct sockaddr*)&servAddr, sizeof(struct sockaddr));
}

int TCPSocket::bind(int port, const char* adres)
{
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(port);
	servAddr.sin_addr.s_addr = inet_addr(adres);
	
	return ::bind(fd, (struct sockaddr*)&servAddr, sizeof(struct sockaddr));
}

int TCPSocket::connect(int port, const char* adres)
{
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = htons(port);
	servAddr.sin_addr.s_addr = inet_addr(adres);
	
	return ::connect(fd, (struct sockaddr*)&servAddr, sizeof(struct sockaddr));
}
int TCPSocket::listen(int queueSize)
{	
	return ::listen(fd, queueSize);
}

int TCPSocket::getFd()
{
	return TCPSocket::fd;
}

void TCPSocket::close()
{ //close() from unistd...
	::close(fd);
}

