package gui;

import java.awt.EventQueue;
import java.awt.FileDialog;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingWorker;
import javax.swing.border.TitledBorder;
import javax.swing.JProgressBar;
import javax.swing.JTextPane;
import javax.swing.JScrollPane;

public class MainWindow {

	JFrame frmMatrixproject;
	JTextField textFieldMatrixA;
	JTextField textFieldMatrixB;	
	JTextField textFieldIP;
	JTextField textFieldPort;
	JTextField textFieldResultLocation;
	JButton btnStart;
	JButton button;
	
	private MainWindowInteraction interaction = new MainWindowInteraction(this); 
	JLabel lblNewLabel_2;
	JPanel panelResultLocation;
	JPanel panelMatrices;
	JTextPane textPaneProgress;
	JProgressBar progressBar;
	private JPanel panel;
	
	private Task task;
	JScrollPane scrollPaneLog;
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frmMatrixproject.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}
	
	
	class Task extends SwingWorker<Void, Void> {
        /*
         * Main task. Executed in background thread.
         */
        @Override
        public Void doInBackground() {
        	
        	try {
				interaction.send();
			} catch (IOException e1) {
				System.out.println("Error while sending message to server! Error: "+e1.getMessage());
				//e1.printStackTrace();
			}
        	
            return null;
        }

        /*
         * Executed in event dispatching thread
         */
        @Override
        public void done() {
            
        }
    }
	
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmMatrixproject = new JFrame();
		frmMatrixproject.setTitle("MatrixProject");
		frmMatrixproject.setBounds(100, 100, 582, 412);
		frmMatrixproject.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmMatrixproject.getContentPane().setLayout(null);
		
		panelMatrices = new JPanel();
		panelMatrices.setBorder(new TitledBorder(null, "Matrices", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelMatrices.setBounds(4, 0, 558, 77);
		frmMatrixproject.getContentPane().add(panelMatrices);
		panelMatrices.setLayout(null);
		
		JButton btnLoadB = new JButton("Load B");
		btnLoadB.setBounds(463, 47, 89, 23);
		panelMatrices.add(btnLoadB);
		
		textFieldMatrixA = new JTextField();
		textFieldMatrixA.setBounds(6, 17, 447, 20);
		panelMatrices.add(textFieldMatrixA);
		textFieldMatrixA.setColumns(10);
		
		textFieldMatrixB = new JTextField();
		textFieldMatrixB.setBounds(6, 48, 447, 20);
		panelMatrices.add(textFieldMatrixB);
		textFieldMatrixB.setColumns(10);
		
		button = new JButton("Load A");
		button.setBounds(463, 16, 89, 23);
		panelMatrices.add(button);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				FileDialog fd =new FileDialog(frmMatrixproject,"Load Matrix A",FileDialog.LOAD);
			    fd.setVisible(true);
			    
			    if (fd.getFile() != null && fd.getDirectory() != null) {
				    String katalog=fd.getDirectory();
				    String plik=fd.getFile();
				    textFieldMatrixA.setText(katalog+plik);
			    }
			}
		});
		btnLoadB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				FileDialog fd =new FileDialog(frmMatrixproject,"Load Matrix B",FileDialog.LOAD);
			    fd.setVisible(true);
			    
			    if (fd.getFile() != null && fd.getDirectory() != null) {
				    String katalog=fd.getDirectory();
				    String plik=fd.getFile();
				    textFieldMatrixB.setText(katalog+plik);
			    }
		}
		});
		
		JPanel panelServerConfig = new JPanel();
		panelServerConfig.setToolTipText("");
		// Adding border to panel
		TitledBorder title = BorderFactory.createTitledBorder("Server configuration");
		panelServerConfig.setBorder(title);
		//---------------------------------
		panelServerConfig.setBounds(349, 130, 207, 90);
		frmMatrixproject.getContentPane().add(panelServerConfig);
		panelServerConfig.setLayout(null);
		
		textFieldIP = new JTextField();
		textFieldIP.setText("192.168.1.203");
		textFieldIP.setBounds(98, 25, 104, 20);
		panelServerConfig.add(textFieldIP);
		textFieldIP.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("IP");
		lblNewLabel_1.setBounds(84, 26, 21, 14);
		panelServerConfig.add(lblNewLabel_1);
		
		textFieldPort = new JTextField();
		textFieldPort.setText("1234");
		textFieldPort.setColumns(10);
		textFieldPort.setBounds(98, 56, 104, 20);
		panelServerConfig.add(textFieldPort);
		
		JLabel lblPort = new JLabel("Port");
		lblPort.setBounds(69, 59, 27, 14);
		panelServerConfig.add(lblPort);
		
		btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				//Tworzenie zadania, w ktorym bedzie wykonana komunikacja z serwerem.
				//Odbywa sie to w osobnym watku, by aplikacji byla responsywna.
				task = new Task();
		        task.execute();
			}
		});
		btnStart.setBounds(467, 234, 89, 23);
		frmMatrixproject.getContentPane().add(btnStart);
		
		panelResultLocation = new JPanel();
		panelResultLocation.setBorder(new TitledBorder(null, "Result location", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panelResultLocation.setBounds(4, 78, 558, 46);
		frmMatrixproject.getContentPane().add(panelResultLocation);
		panelResultLocation.setLayout(null);
		
		textFieldResultLocation = new JTextField();
		textFieldResultLocation.setBounds(6, 17, 447, 20);
		panelResultLocation.add(textFieldResultLocation);
		textFieldResultLocation.setColumns(10);		
		
		textFieldResultLocation.setText(System.getProperty("user.dir")+"\\result.txt");
		
		JButton btnResultLocation = new JButton("Browse...");
		btnResultLocation.setBounds(463, 16, 89, 23);
		panelResultLocation.add(btnResultLocation);
		btnResultLocation.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				interaction.setResultLocation();
			}
		});
		
		lblNewLabel_2 = new JLabel("By Iwo B\u0142\u0105dek & Micha\u0142 Boro\u0144");
		lblNewLabel_2.setBounds(378, 357, 178, 14);
		frmMatrixproject.getContentPane().add(lblNewLabel_2);
		
		progressBar = new JProgressBar();
		progressBar.setBounds(6, 324, 552, 23);
		frmMatrixproject.getContentPane().add(progressBar);
		
		panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Progress", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(4, 182, 325, 136);
		frmMatrixproject.getContentPane().add(panel);
		panel.setLayout(null);
		
		scrollPaneLog = new JScrollPane();
		scrollPaneLog.setBounds(5, 20, 315, 110);
		panel.add(scrollPaneLog);
		
		textPaneProgress = new JTextPane();
		textPaneProgress.setEditable(false);
		scrollPaneLog.setViewportView(textPaneProgress);
		
		
	}
}
