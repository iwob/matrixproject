#include "../includes/tcpsocket.cpp"
#include "../includes/socketIO.cpp"
#include <string.h>
using namespace std;
	
const bool debug = false;
int main(){

	char buf[100];
	int znaki;
	
	string adresIpSerwera;
	
	cout << "Adres serwera: ";
	cin >> adresIpSerwera;

	TCPSocket workerSocket;
	workerSocket.init();
	workerSocket.connect(1234, adresIpSerwera.c_str());		

	writeFixed(workerSocket.getFd(), "WORKER\n", 7);
	znaki =	readSpecialChar(workerSocket.getFd(), buf, 13, '\n');
	buf[znaki] = 0;
	
	if(strncmp(buf,"SORRY",5) == 0) {
		
		cout << "Brak miejsc dla pracownikow pod adresem " << adresIpSerwera << endl;
		return -1;
		
	} else if(strncmp(buf, "HELLO", 5) == 0) {
		
		cout << "Polaczenie zaakceptowane. Oczekiwanie na zlecenie pracy od serwera. " << endl;
		
	} else {
		
		cout << "Nieznany komunikat." << endl;
		return -2;
		
		}

	if(debug)printf("%s\n",buf);


	for(;;){
		
		cout << endl;
		
		int matrixWidth;
		int clientPID;
		int firstRowOfChunk;
		
		int memExpected = 0, memIs = 0;
		
		readFixed(workerSocket.getFd(), &firstRowOfChunk, sizeof(int));	//indeks pierwszego wiersza
		readFixed(workerSocket.getFd(), &clientPID, sizeof(int));		//PID procesu klienta na serwerze
		int s = readFixed(workerSocket.getFd(), &matrixWidth, sizeof(int)); 	//szerokosc macierzy
		
		if(s == 0) {
			
			cout << "Polaczenie zerwane przez serwer." << endl;
			return -3;
			
			}
		
		float *macierz = new float[matrixWidth*matrixWidth];			//alokacja pamieci dla macierzy
		
		memExpected = sizeof(float)*matrixWidth*matrixWidth;
		memIs = readFixed(workerSocket.getFd(), macierz, sizeof(float)*matrixWidth*matrixWidth); //wczytaj macierz B
		
		cout << "Otrzymano dane od serwera." << endl;
		
		
		if(debug) {
			
			cout << "Spodziewano sie: " << memExpected << " bajtow." << endl;
			cout << "Otrzymano: " << memIs << " bajtow." << endl;
		
			for(int i=0; i < matrixWidth; i++) {	//wypisz matrix B
				for(int j=0; j< matrixWidth; j++) {
					cout << macierz[i*matrixWidth + j] << ", ";
				}
				cout << endl;
			}
			cout << endl;
			
		}
		
		int rowCount;
		readFixed(workerSocket.getFd(), &rowCount, sizeof(int)); //ile mamy wierszy z macierzy A
		
		float *rows = new float[matrixWidth*rowCount]; //alokacja pamieci na wiersza macierzy A
		
		readFixed(workerSocket.getFd(), rows, sizeof(float)*matrixWidth*rowCount); //wczytanie wierszy macierzy A
		
		if(debug){											//wypisanie wierszy macierzy A
		
			for(int i=0; i < rowCount; i++) { 			//iteracja po kolumnach
				cout << endl << "kolejny wiersz:  ";
				for(int j=0; j < matrixWidth; j++) { 	//iteracja po elementach kolumny
					cout << rows[i*matrixWidth + j] << ", ";
				}
			}
			cout << endl;
		}
		
		float *result = new float[matrixWidth*rowCount];
		
		//inicjalizacja macierzy result zerami
		for(int i=0; i < matrixWidth*rowCount; i++)result[i] = 0;
		
		
		//iterujemy po wierszach macierzy A
		for(int i=0; i < rowCount; i++) {
			//iterujemy po kolumnach macierzy B
			for(int k=0; k < matrixWidth; k++) {
				//iterujemy po elementach wiersza i kolumny
				for(int j=0; j< matrixWidth; j++) {
					
					result[i*matrixWidth + k] += rows[i*matrixWidth + j] * macierz[j*matrixWidth + k];
					
					}
				}
			}
			
		cout << "Wykonano obliczenia." << endl;
			
		//indeks pierwszego wiersza
		writeFixed(workerSocket.getFd(), &firstRowOfChunk, sizeof(int));
		//PID procesu klienta
		writeFixed(workerSocket.getFd(), &clientPID, sizeof(int));
		//szerokosc macierzy
		writeFixed(workerSocket.getFd(), &matrixWidth, sizeof(int));
		//ilosc wierszy macierzy A
		writeFixed(workerSocket.getFd(), &rowCount, sizeof(int));
		//wynik
		writeFixed(workerSocket.getFd(), result, sizeof(float)*matrixWidth*rowCount);
		
		cout << "Przeslano wynik do serwera." << endl;
		
		if(debug) {
			cout << "*****************************" << endl << "WYNIK" << endl << "**********************" << endl;
			for(int i=0; i < rowCount; i++) {
				for(int j=0; j < matrixWidth; j++) {
						cout << result[i * matrixWidth + j] << ", " ;
					}
					cout << endl;
				}
			}
		
	}

	exit(0);
}
