#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <string.h>


int main()
{
	int sock, wynik, znaki;
	char buf[100];	
	
	struct sockaddr_in servAddr;
	struct hostent *hostName;
	
	sock=socket(AF_INET, SOCK_STREAM, 0);
	servAddr.sin_port=htons(1234);	
	servAddr.sin_addr.s_addr = inet_addr("127.0.0.1");
	servAddr.sin_family=AF_INET;

	wynik=connect(sock, (struct sockaddr*)&servAddr, sizeof(servAddr));
	
	znaki=read(sock, buf, 13);
	buf[znaki]=0;
	printf("%s\n",buf);

	close(sock);
	return;
}

