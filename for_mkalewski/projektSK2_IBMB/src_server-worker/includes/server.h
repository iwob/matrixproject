#ifndef SERVER_H
#define SERVER_H

class ClientCallback {
	public:
  		virtual void clientConnect(int)=0;
};

class Server{

protected:

	TCPSocket serverSock;			//server socket
	int nClientSocket; 			//client socket descriptor

	struct sockaddr_in stClientAddr;	
	socklen_t nSize;

	ClientCallback *clientCallback;


public:
	
	char* getClientAddress();
	virtual void preinit(ClientCallback *cc){clientCallback = cc;}
	int init(int port, int queueSize);
	void run();
};



#endif
