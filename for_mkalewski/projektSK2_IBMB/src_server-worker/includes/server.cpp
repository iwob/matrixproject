#include "tcpsocket.cpp"
#include "server.h"
/*
void childend(int signo)
{
   pid_t pid=0;
   //pid = wait();//NULL
   printf("\t[end of child process number %d]\n", pid);
}
*/

int Server::init(int port, int queueSize){
	int nFoo = 1;
	

	if (serverSock.init() < 0){
		printf("Can't create a socket...\n");
		return -1;
	}	

	setsockopt(serverSock.getFd(), SOL_SOCKET, SO_REUSEADDR, (char*)&nFoo, sizeof(nFoo));

	if (serverSock.bind(port) < 0){
		printf("Can't bind a name to a socket.\n");
		serverSock.close();
		return -1;
	}

	if (serverSock.listen(queueSize) < 0){
		printf("Can't set queue size.\n");
		serverSock.close();
		return -1;
	}		
	
	return 0;
}

void Server::run(){

	while(1)
	{
       	/* block for connection request */
	       	nSize = sizeof(struct sockaddr);
	       	nClientSocket = accept(serverSock.fd, (struct sockaddr*)&stClientAddr, &nSize);
	       	if (nClientSocket < 0)
	       	{
		   	exit(1);
	    	}

			/* connection */
	       	if (! fork())
	       	{
			clientCallback->clientConnect(nClientSocket);
		   	exit(0);

			}
	}
	
	
	serverSock.close();
}

char* Server::getClientAddress(){
	return inet_ntoa((struct in_addr)stClientAddr.sin_addr);
	}
