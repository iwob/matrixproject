#include "socketIO.h"
#include <string.h>


int readFixed(int deskryptor, void *cos, size_t rozmiar){ //read from socket; czyta dokladnie rozmiar
														  //bajtow z socketa, jesli bedzie mniej to sie zawiesi
		char tablica[rozmiar]; //tablica bajtow ktore chcemy odczytac

		unsigned int cummulatedRead = 0;	//wczytujemy dopoki nie oproznimy bufora, albo nie odczytamy ile chcemy
		unsigned int actuallyRead = 0;
		
		do{
			
			actuallyRead = read(deskryptor, tablica + cummulatedRead, rozmiar);
			cummulatedRead += actuallyRead;
			
		} while(actuallyRead != 0 && cummulatedRead < rozmiar);
		
		memcpy((char *)cos, tablica, cummulatedRead);
		
		return cummulatedRead;
	}
	
int writeFixed(int deskryptor, const void *cos, size_t rozmiar){
		char tablica[rozmiar]; //tablica bajtow ktore chcemy odczytac
		
		memcpy(tablica, (char *)cos, rozmiar);
		
		unsigned int cummulatedWrite = 0;	//wczytujemy dopoki nie oproznimy bufora, albo nie odczytamy ile chcemy
		int actuallyWritten = 0;
		
		do{
			actuallyWritten = write(deskryptor, tablica + cummulatedWrite, rozmiar);
			cummulatedWrite += actuallyWritten;
		} while(actuallyWritten != 0 && cummulatedWrite < rozmiar);
		
		
		
		return 0;
	}

int readSpecialChar(int deskryptor, char *cos, size_t rozmiarMax, char specialChar){ //works only if input is a c-string
	
		char tablica[rozmiarMax];
		
		unsigned int cumulatedRead = 0;
		unsigned int actuallyRead = 0;
		unsigned int specialCharPos = 0;
		
		do{
			
			actuallyRead = read(deskryptor, tablica + cumulatedRead, rozmiarMax); //ile wczytalismy tym readem
			
			for(unsigned int i = cumulatedRead; i < cumulatedRead + actuallyRead; i++) {
					if(tablica[i] == specialChar) {
							specialCharPos = i;
							break;
						}
				}
			cumulatedRead += actuallyRead;
			if(specialCharPos != 0)break;
			
			}while(actuallyRead != 0 && cumulatedRead < rozmiarMax);
			
			memcpy(cos, tablica, specialCharPos);
			return cumulatedRead;
	
	}
