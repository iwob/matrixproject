#ifndef SOCKETIO_H
#define SOCKETIO_H

#include<unistd.h>

int readFixed(int deskryptor, void *cos, size_t rozmiar);
	
int writeFixed(int deskryptor, const void *cos, size_t rozmiar);

int readSpecialChar(int deskryptor, char *cos, size_t rozmiarMax, char specialChar);

#endif
