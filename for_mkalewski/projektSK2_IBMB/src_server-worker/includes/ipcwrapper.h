#ifndef IPCWRAPPER_H
#define IPCWRAPPER_H

class IPCWrapper {
	
	private :
		
		int refToSharedMemoryId;
		int refToSemaphoreId;
		
		int createSharedMemoryInternal(size_t size);
		
		
	public : 
	
		int 	createSemaphore();
		int  	createSemaphore(int initVal);
		void 	releaseSemaphore(int semId);
		void 	acquireSemaphore(int semId);
		//int 	getSemaphoreId(int key) {
		
		int 	createSharedMemory(size_t size);
		void * 	getSharedMemoryPointer(int shmId);
		
			
		IPCWrapper();
		~IPCWrapper();
	};

#endif
