#ifndef TCPSOCKET_H
#define TCPSOCKET_H

class TCPSocket{
	private:
	
	struct sockaddr_in servAddr;

	public:
	int fd; //file descriptor
	
	TCPSocket();
	int init();
	int bind(int port);
	int bind(int port, const char* adres);
	int connect(int port, const char* adres);
	int listen(int queueSize);
	int getFd();
	void close();
};


#endif
