#include "../includes/server.cpp"
#include "../includes/ipcwrapper.cpp"
#include "../includes/socketIO.cpp"
#include <string.h>
#include <vector>
const bool debug = false;
void globalPeerDisconnected(int signo);

class TaskMgr : public Server{
	private:		
	
		IPCWrapper ipcwrapper;	//uchwyt do wrappera na IPC
		
		//od 0 do clientLimit-1 pidy klientow; 
  	    //od clientLimit do clientLimit + 
 	    // + workerLimit -1 pidy workerow
		int shmClientsAndWorkers; 
		
		int semClientsAndWorkers;
		
		int semContractWorker; //
		
		int shmClientsShmMatrix; //tablica wskaznikow na pamiec wspoldzielona zawierajaca macierze
		int shmClientsSemMatrix; //tablica zamkow do w/w tablic
		int shmClientsMultiSemComplete; //tablica semaforow wskazujacych stopien ukonczenia prac
		
		int shmWorkersFd;
		int shmWorkersFree; //1 = free; 0 = working/non-existing
		
		int shmWorkerLimit; //ilu robotnikow jest aktualnie podlaczonych
		int shmWorkerCount; //ilu robotnikow moze byc maksymalnie podlaczonych
		
		int shmClientLimit; //ilu klientow jest aktualnie podlaczonych
		int shmClientCount; //ilu klientow moze byc maksymalnie podlaczonych
		
		
		
		void clientService(int clientFd); //obsluga klienta
		void workerService(int workerFd); //obsluga pracownika
		void printConnectedWorkers();
		int connectWorker(int WorkerFd);
		
	
		class TaskMgrCallback : public ClientCallback { //implementacja callback'u
			private:
				TaskMgr &taskmgr;		//referencja na klasę 'wyzej', zeby mozna na niej
										//bylo wykonac niestatyczne metody
			public:
				TaskMgrCallback(TaskMgr &t) : taskmgr(t) {}
				
				int x;
				void clientConnect(int clientFd){ //funkcja wywolywana przez serwer po accept

					char buf[100];
	
					int actuallyRead = readSpecialChar(clientFd, buf, 13, '\n');
					buf[actuallyRead] = 0;	

					if(strncmp(buf,"WORKER",6) == 0){
						taskmgr.workerService(clientFd); //przekazania sterowania do Serwera (tego, który rozdziela pracę)
					}
					else if(strncmp(buf,"CLIENT",6)==0){
						taskmgr.clientService(clientFd);
					}
	
					close(clientFd);
				}
		};
		
		TaskMgrCallback taskMgrCallback; //instancja klasy umozliwiajacej callback
	public:	
	
		void peerDisconnected(int signo); //funkcja wywolywana w momencie zakonczenia sie procesu klienta/pracownika
		
		void setLimits(int ClientLimit, int WorkerLimit) {
				int *clientLimit = (int *) ipcwrapper.getSharedMemoryPointer(shmClientLimit);
				*clientLimit = ClientLimit;
				
				int *workerLimit = (int *) ipcwrapper.getSharedMemoryPointer(shmWorkerLimit);
				*workerLimit = WorkerLimit;
				
				//jedna tablica na pidy wszystkich potomkow ; sluzy do zorientowania sie na podstawie
				//PID czy proces sluzy do obslugi klienta czy robotnika
			
				shmClientsAndWorkers = ipcwrapper.createSharedMemory(sizeof(int)*((*clientLimit) + (*workerLimit)));
				int *clientsAndWorkers = (int *) ipcwrapper.getSharedMemoryPointer(shmClientsAndWorkers);
				
				for(int i=0; i < ((*clientLimit)+(*workerLimit)); i++) {
					clientsAndWorkers[i] = -1;
					}
					
				//wlasciwosci klienta
				
				shmClientsShmMatrix = ipcwrapper.createSharedMemory(sizeof(int) * (*clientLimit));
				shmClientsSemMatrix = ipcwrapper.createSharedMemory(sizeof(int) * (*clientLimit));
				shmClientsMultiSemComplete = ipcwrapper.createSharedMemory(sizeof(int) * (*clientLimit));
				
				
				//wlasciwosci robotnika
				
				shmWorkersFd = ipcwrapper.createSharedMemory(sizeof(int)*(*workerLimit));
				int *workersFd = (int *) ipcwrapper.getSharedMemoryPointer(shmWorkersFd);
				
				for(int i=0; i < (*workerLimit); i++) {
					workersFd[i] = 0;
					}
				
				shmWorkersFree = ipcwrapper.createSharedMemory(sizeof(int)*(*workerLimit));
				int *workersFree = (int *) ipcwrapper.getSharedMemoryPointer(shmWorkersFree);
				
				for(int i=0; i < (*workerLimit); i++) {
					workersFree[i] = 0;
					}
			}
		
		TaskMgr();
};




TaskMgr::TaskMgr() : taskMgrCallback(*this){		//inicjalizacja interfejsu umozliwiajacego callback
		
			signal(SIGCHLD, globalPeerDisconnected);	//rejestracja funkcji wywolywanej po smierci potomka
		
			//alokacja pamieci wspoldzielonych
			
			shmWorkerLimit = ipcwrapper.createSharedMemory(sizeof(int));
			
			shmWorkerCount = ipcwrapper.createSharedMemory(sizeof(int));
			int *workerCount = (int *) ipcwrapper.getSharedMemoryPointer(shmWorkerCount);
			*workerCount = 0;
			
			shmClientLimit = ipcwrapper.createSharedMemory(sizeof(int));
			
			shmClientCount = ipcwrapper.createSharedMemory(sizeof(int));
			int *clientCount = (int *) ipcwrapper.getSharedMemoryPointer(shmClientCount);
			*clientCount = 0;
			
			
			//bariery synchronizacyjne
			
			semClientsAndWorkers = ipcwrapper.createSemaphore();
			semContractWorker = ipcwrapper.createSemaphore();
			
			//zarejestrowanie callbacka w serwerze
			
			Server::preinit(&taskMgrCallback);
		}
		
void TaskMgr::peerDisconnected(int signo) { //wywolywane przez globalPeerDisconnected
		
			ipcwrapper.acquireSemaphore(semClientsAndWorkers); //synchr.
			
			pid_t pid=0;
			pid = wait(NULL);//NULL
			if(debug)printf("\t[end of child process number %d]\n", pid);  //debug
			
			int *clientsAndWorkers = (int *)ipcwrapper.getSharedMemoryPointer(shmClientsAndWorkers);
			int *clientLimit = (int *)ipcwrapper.getSharedMemoryPointer(shmClientLimit);
			int *workerLimit = (int *) ipcwrapper.getSharedMemoryPointer(shmWorkerLimit);
			
			int *clientCount = (int *)ipcwrapper.getSharedMemoryPointer(shmClientCount);
			int *workerCount = (int *)ipcwrapper.getSharedMemoryPointer(shmWorkerCount);
			
			int *workersFd = (int *)ipcwrapper.getSharedMemoryPointer(shmWorkersFd);
			int *workersFree = (int *)ipcwrapper.getSharedMemoryPointer(shmWorkersFree);
			
			for(int i = 0; i < ((*workerLimit)+(*clientLimit)); i++){
				
				if(clientsAndWorkers[i] == pid){
						clientsAndWorkers[i] = -1; //usuwamy wpis z tablic, wazne!
						
						if(i < (*clientLimit)) { //to jest klient
						
							std::cout << "Client disconnected" << std::endl; //debug
							*clientCount = (*clientCount) - 1; //zmniejszamy licznik klientow, wazne!
							
						} else {
							
							ipcwrapper.acquireSemaphore(semContractWorker);
							
							//usuwamy wpisy z tablic
							workersFd[i - (*clientLimit)] = 0;		
							workersFree[i - (*clientLimit)] = 0;
							
							std::cout << "Worker disconnected" << std::endl;
							
							*workerCount = (*workerCount) - 1; //zmniejszamy licznik workerow, wazne!
							
							ipcwrapper.releaseSemaphore(semContractWorker);
						}
					}
					
			}
			
			ipcwrapper.releaseSemaphore(semClientsAndWorkers);
			
			printConnectedWorkers();
			
			}

void TaskMgr::clientService(int clientFd){
			
			int *clientLimit = (int *) ipcwrapper.getSharedMemoryPointer(shmClientLimit);
			int *clientCount = (int *) ipcwrapper.getSharedMemoryPointer(shmClientCount);
			
			ipcwrapper.acquireSemaphore(semClientsAndWorkers);
			
			if((*clientCount) < (*clientLimit)) { //sa jeszcze wolne miejsca
			
				int myPid = getpid();
				int *clientsAndWorkers = (int *)ipcwrapper.getSharedMemoryPointer(shmClientsAndWorkers);
								
				(*clientCount) = (*clientCount) + 1; //zwiekszamy licznik klientow
				
				for(int i = 0; i < (*clientLimit); i++) { //szukamy wolnego miejsca w tablicy PIDow
				
					if(clientsAndWorkers[i] == -1) {
						
						clientsAndWorkers[i] = myPid; //zapisujemy wlasny PID
						break;
						
						}
					
				}
				
				ipcwrapper.releaseSemaphore(semClientsAndWorkers);
			} else { //brak miejsca
				
				std::cout << "Client slots are full, no entry!" << std::endl; //debug
				ipcwrapper.releaseSemaphore(semClientsAndWorkers);
				write(clientFd, "SORRY\n", 6);
				printConnectedWorkers();	//debug
				return;
				
			}
			
			
			const int BUF_READ = 20;
				
			std::cout << "Client connection attempt from: " << getClientAddress() << std::endl; //debug
			//std::cout << "welcome to client service" << std::endl;	//debug

			
			//writeFixed(clientFd, "YES\n\0", 5);	//protocol
			writeFixed(clientFd, "HELLO\n", 6);

			//std::cout << "clientFd = " << clientFd << std::endl; //debug
			//std::cout << "written YES" << std::endl; //debug
			
			int n, m; 	//Rozmiar macierzy, n - wzdluz osi x(kolumny), 
						//m - osi y(wiersze), tab[n][m]
						
			char buf[BUF_READ+1];
			
			//Wczytywanie n	
			
			int actuallyRead = readSpecialChar(clientFd, buf, BUF_READ, '\n'); //protocol
			buf[actuallyRead] = 0;					
			n = atoi(buf);
			
			write(clientFd, "ACK\n", 5); //ACK to client; protocol
			
			if(debug) {
				for(int i = 0; i < actuallyRead; i++) { //debug
					std::cout << buf[i];
				}
				std::cout << std::endl << "actuallyRead n " << actuallyRead << std::endl; //debug
			}
			//Wczytywanie m
			actuallyRead = readSpecialChar(clientFd, buf, BUF_READ, '\n');
			buf[actuallyRead] = 0;					
			m = atoi(buf);
			
			write(clientFd, "ACK\n", 5); //ACK to client
			
			if(debug) {
				std::cout << "actuallyRead m " << actuallyRead << std::endl; //debug
				std::cout << "read: n=" << n << std::endl;
				std::cout << "read: m=" << m << std::endl;
			}
			
			//Reading matrix A
			
			float *matrixA = new float[n*n]; //alocating space for matrices
			float *matrixB = new float[n*n];
						
			for (int i=0; i<n; i++)
			for (int j=0; j<n; j++)
			{
				actuallyRead = readSpecialChar(clientFd, buf, BUF_READ, '\n'); //protocol
				buf[actuallyRead] = 0;
				write(clientFd, "ACK\n", 5); //ACK to client; protocol
				
				matrixA[i*n + j] = atof(buf);
			}
			
			//Readin matrix B
			for (int i=0; i<n; i++)
			for (int j=0; j<n; j++)
			{
				actuallyRead = readSpecialChar(clientFd, buf, BUF_READ, '\n'); //protocol
				buf[actuallyRead] = 0;
				writeFixed(clientFd, "ACK\n", 5); //ACK to client; protocol
			
				matrixB[i*n + j] = atof(buf);
			}
			
			
			//Printing matrix A
			std::cout << std::endl;
			std::cout << "Matrix A:\n";
			for (int i=0; i<n; i++){
				for (int j=0; j<n; j++){
					std::cout << matrixA[i*n + j] << " ";
				}
				std::cout << std::endl;
			}
			
			//Printing matrix B
			std::cout << std::endl;
			std::cout << "Matrix B:\n";
			for (int i=0; i<n; i++){
				for (int j=0; j<n; j++){
					std::cout << matrixB[i*n +j] << " ";
				}
				std::cout << std::endl;
			}
			
			if(debug) std::cout << "checking available workers" << std::endl; //debug
			
			int *workersFree = (int *)ipcwrapper.getSharedMemoryPointer(shmWorkersFree);
			int *workersFd = (int *)ipcwrapper.getSharedMemoryPointer(shmWorkersFd);
			int *workerLimit = (int *)ipcwrapper.getSharedMemoryPointer(shmWorkerLimit);
			
			ipcwrapper.acquireSemaphore(semContractWorker);
			
			if(debug) {
				
				for(int i=0; i < (*workerLimit) ; i++) { //debug
					std::cout << workersFree[i] << ", ";
				}
				std::cout << std::endl;
				
				std::cout << "hiring workers" << std::endl; //debug
				
			}
			int jobSize = n; //ilosc pracy do podzialu
			int hiredWorkers = 0; //ilosc zatrudnionych robotnikow
			
			int *hiredWorkersFd = new int[(*workerLimit)];
			for(int i=0; i<(*workerLimit); i++)hiredWorkersFd[i] = 0;
			
			for(int i=0; i < (*workerLimit) ; i++) {	//iterujemy po workersFree, 1 oznacza ze robotnik jest do wziecia
				
					if(workersFree[i] == 1) {
						workersFree[i]=0; 					//bierzemy robotnika, wiec nikt inny nie moze go uzyc
						hiredWorkersFd[i] = workersFd[i]; 	//przepisujemy sobie deskryptor robotnika
						hiredWorkers++; 					//zwiekszamy licznik zatrudnionych robotnikow
					}
					
					if(hiredWorkers >= jobSize)break;		//nie mozemy zatrudnic wiecej robotnikow niz mamy pracy
				}
				
			if(debug) {
				//debug
				std::cout << std::endl;
				std::cout << "free workers remaining: " << std::endl;
				for(int i=0; i < (*workerLimit) ; i++) {	
						std::cout << workersFree[i] << ", ";
					}
				std::cout << std::endl;
				
				//debug
				std::cout << "hired " << hiredWorkers << " workers: " << std::endl;
				for(int i=0; i < (*workerLimit) ; i++) {
						if(hiredWorkersFd[i] != 0)std::cout << hiredWorkersFd[i] << ", ";
					}
				std::cout << std::endl;
			}
			ipcwrapper.releaseSemaphore(semContractWorker);
			
			ipcwrapper.acquireSemaphore(semClientsAndWorkers);
			
			//alokacja pamieci wspoldzielonej na wyniki
			
				int shmMatrix = ipcwrapper.createSharedMemory(sizeof(float)*jobSize*jobSize);
				int semMatrix = ipcwrapper.createSemaphore();
				int semMultiComplete = ipcwrapper.createSemaphore(0);
				
				int * clientsAndWorkers = (int *)ipcwrapper.getSharedMemoryPointer(shmClientsAndWorkers);
				int myPid = getpid();
				
				
				for(int i = 0; i < (*clientLimit); i++){ //szukamy swoich wlasciwosci we wspoldzielonych tabelach
				
					if(clientsAndWorkers[i] == myPid) {
						
						int *clientsShmMatrix = (int *)ipcwrapper.getSharedMemoryPointer(shmClientsShmMatrix);
						int *clientsSemMatrix = (int *)ipcwrapper.getSharedMemoryPointer(shmClientsSemMatrix);
						int *clientsMultiSemComplete = (int *)ipcwrapper.getSharedMemoryPointer(shmClientsMultiSemComplete);
						
						clientsShmMatrix[i] = shmMatrix;	//identyfikator pamieci wspoldzielonej z macierza wynikowa
						clientsSemMatrix[i] = semMatrix;	//zamek w/w macierzy
						clientsMultiSemComplete[i] = semMultiComplete;	//semafor synchronizujacy zakonczenie pracy watku klienta
																		//opuszczany tyle razy ile jest robotnikow
																		//kazdy robotnik podnosi po przeslaniu wyniku
						break;
						
						}
					
				}
			
			
			ipcwrapper.releaseSemaphore(semClientsAndWorkers);
			
			//jobSize => ilosc pracy, ktora zostala do podzialu
			//hiredWorkers => ilosc jeszcze nie przydzielonych robotnikow
			
			int workChunk; 					//ilosc wierszy, ktora zostanie przydzielona kolejnemu pracownikowi
			int firstRowOfWorkChunk = 0;	//indeks pierwszego wiersza przydzielonego nastepnemu prac.
			const int HIRED_WORKERS = hiredWorkers; //calkowita liczba pracownikow
			
			for(int i=0; i < (*workerLimit) ; i++) {
					
					if(hiredWorkersFd[i] != 0) { //mamy workera
					
						if(jobSize % hiredWorkers == 0)workChunk = jobSize / hiredWorkers; //dzielimy prace rownomiernie
						else workChunk = (jobSize / hiredWorkers) + 1;
						
						jobSize -=workChunk;	//dekrementujemy ilosc pracy, ktora pozostala
						hiredWorkers--;			//dekrementujemy ilosc wolnych pracownikow
						
						if(debug)std::cout << "workChunk = " << workChunk << std::endl; //debug
					
						int workerFd = hiredWorkersFd[i];
						int matrixWidth = n;	
						
						//przesylamy pid klienta (pozniej worker service odbierajacy wyniki bedzie wiedzial komu je przekazac)
						int mypid = getpid();  
						writeFixed(workerFd, &firstRowOfWorkChunk, sizeof(int));	//protocol
						writeFixed(workerFd, &mypid, sizeof(int));					//indeks 1 wiersza, pid proc. klienta
						writeFixed(workerFd, &matrixWidth, sizeof(int));			//szer. macierzy, macierz B
						writeFixed(workerFd, matrixB, sizeof(float)*matrixWidth*matrixWidth); 
						
						
						writeFixed(workerFd, &workChunk, sizeof(int)); //ilosc wierszy macierzy A; macierz A
						writeFixed(workerFd, matrixA+firstRowOfWorkChunk*matrixWidth, sizeof(float)*matrixWidth*workChunk);
						
						firstRowOfWorkChunk += workChunk;	//przesuwamy wskaznik pierwszego wiersza
						
						}
				}
				
				for(int i=0; i < HIRED_WORKERS; i++){
						ipcwrapper.acquireSemaphore(semMultiComplete);  //czekamy na zakonczenie pracy robotnikow
					}
				
				std::cout << std::endl <<  "Wynik: " << std::endl; //debug
				
				float *matrix = (float *)ipcwrapper.getSharedMemoryPointer(shmMatrix); //wynik
				for(int i=0; i < n; i++) {
					
					for(int j=0; j < n; j++) {
							std::cout << matrix[i*n + j] << ", ";
						}
						std::cout << std::endl;
					}
				
				//WYSYLANIE WYNIKOW DO KLIENTA
				
				writeFixed(clientFd, "RESULT\n", 7);
				
				std::cout << std::endl << "sending result to client" << std::endl << std::endl;
				
				char buffer [50];
				for(int i=0; i < n; i++) {
					if(debug)std::cout << "write for i = " << i << std::endl;
										
					for(int j=0; j < n; j++) {
							int nb = sprintf (buffer, "%f\n", matrix[i*n + j]);
							
							if (debug) {
								std::cout << "buffer = ";
								for (int k=0; k<nb; k++)
									std::cout << buffer[k];
								std::cout << " , value = " << matrix[i*n + j] << std::endl;		
							}
							writeFixed(clientFd, (void *)buffer, nb);
						}
					}
				
				
				//read(clientFd, buforr, 100); //blokada trzymajaca proces przy zyciu
				
			
		}


void TaskMgr::printConnectedWorkers(){ //debug
		
		int *clientsAndWorkers = (int *)ipcwrapper.getSharedMemoryPointer(shmClientsAndWorkers);
		int *clientLimit = (int *)ipcwrapper.getSharedMemoryPointer(shmClientLimit);
		int *workerLimit = (int *) ipcwrapper.getSharedMemoryPointer(shmWorkerLimit);
		
		int beginning = (*clientLimit);
		
		std::cout << "Currently connected workers: ";
		
		int counterWorkers = 0;
		
		for(int i=beginning; i < ((*workerLimit)+(*clientLimit)); i++){
			
			if(clientsAndWorkers[i] != -1) {
				std::cout << clientsAndWorkers[i] << ", ";
				counterWorkers++;
				}
				
			}
		if(counterWorkers == 0)std::cout << "none :(" << std::endl;
		else std::cout << std::endl;
			
	}

int TaskMgr::connectWorker(int workerFd) {
	
	//std::cout << "Connection attempt from: " << getClientAddress() << std::endl;
	ipcwrapper.acquireSemaphore(semClientsAndWorkers);
	
	int *workerLimit = (int *) ipcwrapper.getSharedMemoryPointer(shmWorkerLimit);
	int *workerCount = (int *) ipcwrapper.getSharedMemoryPointer(shmWorkerCount);
	int myPid = getpid();
	
	if(*workerCount < *workerLimit) {	//sa dostepne miejsca dla pracownikow
			//std::cout << "Adding new worker!" << std::endl;
			
			ipcwrapper.acquireSemaphore(semContractWorker);
			
			*workerCount = (*workerCount) + 1;
			
			int *clientsAndWorkers = (int *)ipcwrapper.getSharedMemoryPointer(shmClientsAndWorkers);
			int *clientLimit = (int *)ipcwrapper.getSharedMemoryPointer(shmClientLimit);
			
			int *workersFd = (int *)ipcwrapper.getSharedMemoryPointer(shmWorkersFd);
			int *workersFree = (int *)ipcwrapper.getSharedMemoryPointer(shmWorkersFree);
			
			
			for(int i = (*clientLimit); i < ((*workerLimit)+(*clientLimit)); i++) { //szukamy wolnego miejsca w tablicy PIDow
				
				if(clientsAndWorkers[i] == -1){
					
					clientsAndWorkers[i] = myPid;				//wpisujemy wlasny PID
					workersFd[i - (*clientLimit)] = workerFd;	//uzupelniamy tablice deskryptorow
					workersFree[i - (*clientLimit)] = 1;		//uzupelniamy tablice dostepnych pracownikow
					break;
					
					}
				
				}
			ipcwrapper.releaseSemaphore(semContractWorker);
			ipcwrapper.releaseSemaphore(semClientsAndWorkers);
			
			printConnectedWorkers();  //debug
	} else {	//brak miejsc dla klientow
			std::cout << "Worker slots are full, no entry!" << std::endl;
			ipcwrapper.releaseSemaphore(semClientsAndWorkers);
			writeFixed(workerFd, "SORRY\n", 6);		//#TODO
			printConnectedWorkers();
			return 1;
	}
	
	
	return 0;
	}
void TaskMgr::workerService(int workerFd){
			
			if(connectWorker(workerFd) == 1)return;	//brak miejsc dla pracownikow -> wyjscie
			
			writeFixed(workerFd, "HELLO\n", 6); //protocol
			
			int clientPID = 0;
			int rowCount = 0;
			int matrixWidth = 0;
			int firstRowOfChunk = 0;
			
			int *workersFd = (int *)ipcwrapper.getSharedMemoryPointer(shmWorkersFd);
			int *workersFree = (int *)ipcwrapper.getSharedMemoryPointer(shmWorkersFree);
			int *workerLimit = (int *)ipcwrapper.getSharedMemoryPointer(shmWorkerLimit);
			int *clientLimit = (int *)ipcwrapper.getSharedMemoryPointer(shmClientLimit);
			
while(true) {
			
		
			readFixed(workerFd, &firstRowOfChunk, sizeof(int));	//indeks pierwszego wiersza wyniku
			readFixed(workerFd, &clientPID, sizeof(int));		//pid procesu klienta
			readFixed(workerFd, &matrixWidth, sizeof(int));		//szerokosc macierzy
			readFixed(workerFd, &rowCount, sizeof(int));		//ilosc wierszy wyniku
			
						
			float *result = new float[rowCount * matrixWidth];	//alokacja pamieci na wynik
						
			int ar = readFixed(workerFd, result, sizeof(float)*matrixWidth*rowCount); //wczytanie wyniku
			if(ar ==0)break;	//to znaczy ze nic nie przeczytalismy i polaczenie zostalo zerwane
			
			//dostalem wynik i chce go przekazac klientowi:
			
			ipcwrapper.acquireSemaphore(semClientsAndWorkers);
			
			if(debug) {
				
				std::cout << "Wiersze od " << firstRowOfChunk << " do " << firstRowOfChunk + rowCount - 1 << std::endl; //debug
				
				//wypisanie wyniku
				for(int i=0; i<rowCount; i++) { //iteracja po wierszach
					
					for(int k=0; k < matrixWidth; k++) {
						
							std::cout << result[i*matrixWidth + k] << ", ";

						}
					std::cout << std::endl;
					}
				std::cout << std::endl;
				
			}
			
			//zdobycie pamieci wspoldzielonej i semafora
			
			int *clientsShmMatrix = (int *) ipcwrapper.getSharedMemoryPointer(shmClientsShmMatrix);
			int *clientsSemMatrix = (int *)ipcwrapper.getSharedMemoryPointer(shmClientsSemMatrix);
			int *clientsAndWorkers = (int *)ipcwrapper.getSharedMemoryPointer(shmClientsAndWorkers);
			int *clientsMultiSemComplete = (int *)ipcwrapper.getSharedMemoryPointer(shmClientsMultiSemComplete);
			
			int shmMatrix;
			int semMatrix;
			int semMulti;
			
			for(int i=0; i < (*clientLimit); i++){
					if(clientsAndWorkers[i] == clientPID) {
						
							shmMatrix = clientsShmMatrix[i];
							semMatrix = clientsSemMatrix[i];
							semMulti = clientsMultiSemComplete[i];
						
						}
				}
			
			ipcwrapper.releaseSemaphore(semClientsAndWorkers);
			
			ipcwrapper.acquireSemaphore(semMatrix); //przeslanie wynikow do pamieci wspoldzielonej
				
				float *matrix = (float *)ipcwrapper.getSharedMemoryPointer(shmMatrix);
				memcpy(matrix + matrixWidth*firstRowOfChunk, result, sizeof(float)*matrixWidth*rowCount);
				
			ipcwrapper.releaseSemaphore(semMatrix);
			ipcwrapper.releaseSemaphore(semMulti); //dajemy znac ze jeden chunk pracy zostal wykonany
				

			ipcwrapper.acquireSemaphore(semContractWorker);
			for(int i=0; i < (*workerLimit); i++) { //zwalniamy robotnika
					if(workersFd[i] == workerFd) {
						workersFree[i] = 1;
						break;
					}
				}
								
			
			ipcwrapper.releaseSemaphore(semContractWorker);	
			
						
		}
			
	}
	
TaskMgr taskmgr; //instancja naszego serwera


int main(){
	
	int limitKlientow, limitPracownikow;
	
	std::cout << "Lmit klientow: ";
	std::cin >> limitKlientow;
	std::cout << "Limit pracownikow: ";
	std::cin >> limitPracownikow;
	taskmgr.setLimits(limitKlientow,limitPracownikow);
	taskmgr.init(1234, 5);	//inicjalizujemy serwer na porcie 1234
	taskmgr.run();			//uruchamiamy serwer

	exit(0);
}

void globalPeerDisconnected(int signo)
{
   taskmgr.peerDisconnected(signo); //uruchamiamy odpowiednia funkcje serwera
}
