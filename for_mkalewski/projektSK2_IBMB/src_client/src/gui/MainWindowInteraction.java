package gui;

import java.awt.FileDialog;
import java.io.*;
import java.net.Socket;


public class MainWindowInteraction {
	MainWindow MW;
	
	public MainWindowInteraction(MainWindow mw) {
		MW = mw;
	}
	
	/**
	 * Dodaje log do listy.
	 * @param mess
	 */
	void addLogMessage(String mess) {
		MW.textPaneProgress.setText(MW.textPaneProgress.getText() + mess + "\n");
		//MW.scrollPaneLog.setValue( MW.scrollPaneLog.get );
		MW.textPaneProgress.setCaretPosition(MW.textPaneProgress.getDocument().getLength());
		System.out.println(mess);
	}
	
	/**
	 * Usuwa wszystkie logi.
	 */
	void clearLog() {
		MW.textPaneProgress.setText("");
	}
	
	/**
	 * Inicjalizuje pasek postepu obliczen.
	 * @param max
	 */
	void initProgressBar(int max) {
		MW.progressBar.setIndeterminate(false);
		MW.progressBar.setMaximum(max);
		MW.progressBar.setMinimum(0);
		MW.progressBar.setValue(0);
		MW.progressBar.setStringPainted(true);
		MW.progressBar.setVisible(true);
	}
	
	/**
	 * Zwieksza o 1 wartosc paska postepu.
	 */
	void incProgressBar() {
		if (MW.progressBar.getValue()+1 <= MW.progressBar.getMaximum())
			MW.progressBar.setValue(MW.progressBar.getValue()+1);
	}

	/**
	 * Czysci pasek postepu. Przelacza w daterminate mode.
	 */
	void clearProgressBar() {
		MW.progressBar.setIndeterminate(false);
		MW.progressBar.setStringPainted(false);
		MW.progressBar.setValue(0);
	}
	
	/**
	 * Zmienia pasek postepu z procentowego na indeterminate.
	 */
	void inderminateProgressBar() {
		MW.progressBar.setIndeterminate(true);
		MW.progressBar.setStringPainted(false);
	}
	
	/**
	 * Wysyla macierze do serwera i oczekuje na odpowiedz. Po jej przyjsciu zapisuje wynik do pliku.
	 * @throws IOException
	 */
	public void send() throws IOException {
		Socket mySocket = null;
		PrintWriter out = null;
		
		String matrixAfileName = MW.textFieldMatrixA.getText();
		String matrixBfileName = MW.textFieldMatrixB.getText();	
		String resultFileName = MW.textFieldResultLocation.getText();
		String serverIP = MW.textFieldIP.getText();
		String serverPort = MW.textFieldPort.getText();		
		String serverResponse;
		
		
		clearLog();
		addLogMessage("Initializing connection with server...");
		inderminateProgressBar();
		
		BufferedReader inbf = null;
		
		try {
			mySocket = new Socket(serverIP, Integer.parseInt(serverPort));
			inbf = new BufferedReader( new InputStreamReader(mySocket.getInputStream()));
			//DataInputStream in = new DataInputStream(mySocket.getInputStream());
			out = new PrintWriter(mySocket.getOutputStream(), true); //1234 - port nr 
			
			
			out.println("CLIENT");	//Wyslanie zadania przetworzenia macierzy			
			
			serverResponse = inbf.readLine();
			
			
			if (serverResponse.equals("SORRY")) {
				addLogMessage("Server is busy. Try again later.");
				throw new Exception();
			} else if(serverResponse.equals("HELLO")){
				addLogMessage("Connection granted");
			} else {
				addLogMessage("Unrecognized message");
				throw new Exception();
			}
			
			
			addLogMessage("Sending matrix A...");
				
			BufferedReader inputA = new BufferedReader(new FileReader(matrixAfileName));
			BufferedReader inputB = new BufferedReader(new FileReader(matrixBfileName));
			
			int n, m;
			String sLine = inputA.readLine();
			
			try {
				//Wysylanie macierzy A
				
				System.out.println("read from file : "+sLine);			
				String[] parts_nm = sLine.split("\\s");
							
				//Przesylanie n
				n = Integer.parseInt(parts_nm[0]);
				initProgressBar(n*n+2);			
				out.println(parts_nm[0]);
				inbf.readLine(); //ACK from server
				System.out.println("written "+parts_nm[0]);
				incProgressBar();
				
				//Przesylanie m			
				m = Integer.parseInt(parts_nm[1]);
				out.println(parts_nm[1]);
				inbf.readLine(); //ACK from server
				System.out.println("written "+parts_nm[1]);
				System.out.println("n="+n+", m="+m);
				incProgressBar();			
				
				//Przesylanie macierzy A
				for (int i=0; i<n; i++) {				
					sLine = inputA.readLine();
					String[] parts = sLine.split("\\s");
					for (int j=0; j<parts.length; j++) {
						out.println(parts[j]);
						inbf.readLine(); //ACK from server	
						incProgressBar();
					}				
				}//------------------------------------------
				
				
				addLogMessage("Sending matrix B...");
				initProgressBar(n*n);
			
				//Przesylanie macierzy B
				sLine = inputB.readLine();	//na naglowek z rozmiarem macierzy		
				
				for (int i=0; i<n; i++) {				
					sLine = inputB.readLine();
					String[] parts = sLine.split("\\s");
					for (int j=0; j<parts.length; j++) {
						out.println(parts[j]);
						inbf.readLine(); //ACK from server
						incProgressBar();
					}				
				}//------------------------------------------
			}
			finally {
				inputA.close();
				inputB.close();				
			}
			
			
			
		
			inderminateProgressBar();
			addLogMessage("Waiting for results...");		
			
		
							
			sLine = inbf.readLine(); //RESULT or ABORT from server
			
			
			
			addLogMessage("Answer from server: *"+sLine+"*");
		
	
		
			if (sLine.contains("ABORT")) {
				addLogMessage("Operation aborted by server");
				return;
			}
			else if (sLine.contains("RESULT")) 
			{
				//Czytanie wyniku od serwera i zapis do pliku...
				
				
				addLogMessage("Loading result matrix from server...");
		
		
				BufferedWriter save = new BufferedWriter(new FileWriter( resultFileName ));
								
				try {
					
					save.write(n + " " + n);
					save.newLine();
					initProgressBar(n*n);
					
					for (int i=0; i<n; i++) {
						for (int j=0; j<n; j++) {
							String s = inbf.readLine();
							save.write(s+" ");
							incProgressBar();
						}			
						
						save.newLine();
					}					
					
				}
				finally {		
					System.out.println("Closing...");
					save.close();
				}			
			}		
			
			
			addLogMessage("Saved in specified file");
				
					
		}
		catch (Exception e) {
			addLogMessage("Error: "+e.getMessage());
		}
		finally {
			clearProgressBar();
			
			System.out.println("Closing resources...");
			inbf.close();
			out.close();
			mySocket.close();
		}
	}
	
	
	/**
	 * Tworzy dialog, w ktorym uzytkownik wybiera plik w ktorym chce zapisac wynik.
	 * Jezeli w pliku cos bylo, to jest to usuwane.
	 */
	public void setResultLocation() {
		FileDialog fd =new FileDialog(MW.frmMatrixproject, "Destination for Matrix C", FileDialog.SAVE);
		//fd.setDirectory(MW.textFieldResultLocation.getText());
	    fd.setVisible(true);
	    
	    
	    if (fd.getFile() != null && fd.getDirectory() != null) {	    	
	    	String katalog=fd.getDirectory();
		    String plik=fd.getFile();
		    MW.textFieldResultLocation.setText(katalog+plik);
		    
		    //Tworzenie pliku
		    File f = new File(katalog+plik);
		    try {
		    	if (f.exists())
		    		f.delete();
				f.createNewFile();
				
			} catch (IOException e) {
				System.out.println("Couldn't create file. Message="+e.getLocalizedMessage());
			}
	    	
	    }
	    
	    
	}
}
