#include "../includes/tcpsocket.cpp"
#include "../includes/ipcwrapper.cpp"
#include <string.h>
using namespace std;

int main(){
	
	IPCWrapper ipcwrapper;
	int semafor = ipcwrapper.createSemaphore(0);
	
	if(!fork()){
			
			cout << "1" << endl;
				ipcwrapper.releaseSemaphore(semafor);
			cout << "2" << endl;
				ipcwrapper.releaseSemaphore(semafor);
				sleep(1);
			cout << "3" << endl;
				ipcwrapper.releaseSemaphore(semafor);
		
	} else {
		
			ipcwrapper.acquireSemaphore(semafor);
			ipcwrapper.acquireSemaphore(semafor);
			ipcwrapper.acquireSemaphore(semafor);
			cout << endl << "yo ho ho, ostatni" << endl;
		
	}
	return 0;
}
