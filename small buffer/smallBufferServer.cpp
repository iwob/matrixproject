#include "../includes/server.cpp"
#include <string.h>
using namespace std;


class TaskMgr : public Server{
	private:		
		
		class TaskMgrCallback : public ClientCallback { //implementation of callback interface
			private:
				TaskMgr &taskmgr;		//reference to 'upper' class (non-static 
								//methods cannot be called without instance)
			public:
				TaskMgrCallback(TaskMgr &t) : taskmgr(t) {}
				int x;
				void clientConnect(int clientFd){ //determine who is connected and run appropriate function

					cout << "Connection accepted!" << endl;
					char buf[100];
	
					int actuallyRead = read(clientFd, buf, 10);
					buf[actuallyRead] = 0;	

					if(strncmp(buf,"WORKER",6) == 0){
						//taskmgr.workerService(clientFd);
						
						write(clientFd, "abcd\n efghi\n", 15);
						//int asdf = 45;
						//write(clientFd, &asdf, sizeof(int));
					}
					else if(strncmp(buf,"CLIENT",6)==0){
						//taskmgr.clientService(clientFd);
					}
	
					close(clientFd);
				}
		};
		
		TaskMgrCallback taskMgrCallback;
		
	public:	
		
		TaskMgr();
};


TaskMgr::TaskMgr() : taskMgrCallback(*this){		//initializing callback interface
			Server::preinit(&taskMgrCallback);
		}
TaskMgr taskmgr;
int main() {
	
	taskmgr.init(1236, 5);
	taskmgr.run();
	
	return 0;
	}
		
