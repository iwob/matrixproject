#include <iostream>
#include "../includes/ipcwrapper.cpp"

using namespace std;

int main(){
	
	IPCWrapper ipcwrapper;	//this line should appear before first fork
	
	//*****************************
	//SEMAPHORES (initialized to 1)
	//*****************************
	
	//CREATING
	//this integer will be passed to child processes
	//as handler for semaphore
	int semId = ipcwrapper.createSemaphore();
	
	//ACQUIRING
	//hangs if semaphore is not available
	ipcwrapper.acquireSemaphore(semId);
	
	//RELEASING
	//increases semaphore value (even if it's already positive)
	ipcwrapper.releaseSemaphore(semId);
	
	
	//*****************************
	//SHARED MEMORY
	//*****************************
	
	//CREATING
	//this integer will be passed to child processes
	//as handler for shared memory
	int shmId = ipcwrapper.createSharedMemory(sizeof(int));
	
	//GETTING VALUE HANDLE
	
	int *handle = (int *)ipcwrapper.getSharedMemoryPointer(shmId);
	
	//Changes to handle are visible to other processes with access
	//to this shared memory. 
	//Example:
	
	*handle = 8;
	
	
	
	//*****************************
	//EXAMPLE
	//*****************************
	
	//parent and child are having a race
	//parent loses on purpose
	
	int semPierwszy = ipcwrapper.createSemaphore();
	int shmPierwszy = ipcwrapper.createSharedMemory(sizeof(int));
	int *val0 = (int *)ipcwrapper.getSharedMemoryPointer(shmPierwszy);
	
	*val0 = -1;
	
	if(!fork()){
		
		int *val = (int *)ipcwrapper.getSharedMemoryPointer(shmPierwszy);
		
		ipcwrapper.acquireSemaphore(semPierwszy);
		
		cout << "Child process..." << endl;
		if(*val == -1)cout << "First!" << endl << endl;
		else if(*val == 0)cout << "Second!" << endl << endl;
		else cout << "dafuq?" << endl << endl;
		
		*val = 1;
		
		ipcwrapper.releaseSemaphore(semPierwszy);
		
	} else {
		
		sleep(1);
		
		int *val = (int *)ipcwrapper.getSharedMemoryPointer(shmPierwszy);
		
		ipcwrapper.acquireSemaphore(semPierwszy);
		
		cout << "Parent process..." << endl;
		if(*val == -1)cout << "First!" << endl << endl;
		else if(*val == 1)cout << "Second!" << endl << endl;
		else cout << "dafuq?" << endl << endl;
		
		*val = 0;
		
		ipcwrapper.releaseSemaphore(semPierwszy);
		
	}
	
	return 0;
	}

