#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

using namespace std;

int main()
{
    int M, N;

    srand(time(0));

    cout << "rows N = ";
    cin >> N;

    cout << "columns M = ";
    cin >> M;

    float matrix[N][M];

    for (int i=0; i<N; i++)
    for (int j=0; j<M; j++)
        matrix[i][j] = float(rand() % 10) + float(rand()%100000) / 100000;

    string filename;
    cout << "file name = ";
    cin >> filename;
    ofstream out;
    out.open(filename.c_str());

    out << N << " " << M << endl;

    for (int i=0; i<N; i++) {
        for (int j=0; j<M; j++)
            out << matrix[i][j]<<" ";
        out << endl;
    }

    out.close();
}
